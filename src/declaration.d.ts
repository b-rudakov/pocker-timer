declare module '*.scss';
declare module '*.svg?react';

declare const application: import('@hotwired/stimulus').Application;

declare const axios: import('axios').Axios;

declare const _currentRoute: string;

declare const routes: {
    [k: string]: string
};

declare interface ITag {
    id: number
    name: string
    sys_company_id: number
    created_at: string
    updated_at: string
}

declare interface IPaginated<T = any> {
    data: T[];

    current_page: number
    first_page_url: string,
    from: number
    last_page: number
    last_page_url: string
    links: any[],

    next_page_url?: string
    path: string
    per_page: number
    prev_page_url?: string
    to: number,
    total:number,
}

declare type IPossiblePaginated<T = any> = IPaginated<T> | T[];

declare interface IBalance {
    amount: number
    income: number
    output: number
    date: number
}
declare type IBalances = {[k: string]: IBalance[]}
declare interface IWallet {
    availableAmount: number
    account_number?: string
    config: never
    created_at: string
    deleted_at?: string
    hash?: string
    id: number
    info: never
    is_hidden: boolean
    is_manual: boolean
    name: string
    ownerable: any
    ownerable_id: number
    ownerable_type: string
    type: never
    updated_at: string
}
declare interface ITransaction {
    id: number
    amount: number
    description: string

    input_id: number
    output_id: number

    tags: {
        id: number,
        name: string,
    }[]

    performed_at: string
    updated_at: string
    created_at: string
    deleted_at?: string


    hash?: string
    sys_company_id: number
    original_transaction: any
}

