import {SecondaryButton} from "./Fields/Button";
import React from "react";
import {useSideModal} from "./Modal/SideModal";
import {FancyButton} from "./Fields/FancyButton";

export const Settings = () => {
  const {open: openGrid} = useSideModal('blindsGrid');
  return <div className='d-flex flex-column flex-wrap' style={{rowGap: 15, gap: 15}}>
    <FancyButton color='info' onClick={openGrid}>
      ⚙
    </FancyButton>
  </div>
}
